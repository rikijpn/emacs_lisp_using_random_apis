(load "json")

(defun get-string-from-file (file-path)
  (with-temp-buffer
    (insert-file-contents file-path)
    (buffer-string)))

(defun call-api-json (base-url params auth-token)
  "gets the page and returns the plan json output"
  (let* (
	 (uri-to-use (concat base-url params))
    	 (url-request-method "GET")
	 slack-returned-string
    	 )
    (let* ((url-to-use (url-generic-parse-url uri-to-use))
    	   (url-request-extra-headers `(
    	   				("Authorization" .  ,auth-token)
    	   				("Accept" .  "application/json")
    	   				("Content-Type" . "application/json")
    	   				))
    	   (buffer (url-retrieve-synchronously url-to-use)))
      (with-current-buffer buffer
    	(progn
    	  (search-forward "{" nil t)
    	  (beginning-of-line 1)
    	  (let*
    	      (
    	       (ugly-string-beginning (point))
    	       (ugly-string-end (buffer-end 1))
    	       (json-stuff (buffer-substring ugly-string-beginning ugly-string-end)))
	    json-stuff
	    ))))))

;; you can get all dashboards in a folder like this
;; get the dashboards list from NET
;; to get the folderId you need to download a dashboard, the id will be
;; in the metadata
(setq temp-dashboards-list
      (let ((auth (concat "Bearer "
			  (car (split-string (get-string-from-file "auth.txt")))))
	    (api-base-url "https://YOUR-GRAFANA-SERVER/api/")
	    )
	(let* ((api-param (concat "search?folderIds=XXX"))
	       (json-contents (call-api-json api-base-url api-param auth)))
	  (json-read-from-string json-contents)
	  )))



(let ((auth (concat "Bearer "
		    (car (split-string (get-string-from-file "auth.txt"))))) ;; please make this file with the token
      (api-base-url "https://YOUR-GRAFANA-SERVER/api/")
      (dashboard-ids (mapcar (lambda (el) (alist-get 'uid el)) temp-dashboards-list))
      ;; (dashboard-ids ;; you can use this to get a specific dashboard first
      ;;  '(
      ;; 	 "XXXXX" ;; your dashboard ids here, refer to the README about how to get them
      ;; 	 ))
      )
  (dolist (dashboard-id dashboard-ids)
    (let* ((api-param (concat "dashboards/uid/" dashboard-id))
	   (backup-file-name (concat "backup/" dashboard-id ".json"))
	   (json-contents (call-api-json api-base-url api-param auth)))

      (with-temp-buffer
	(insert json-contents)

	;; get a pretty json file instead of a long line super ugly one
	(json-pretty-print 1 (point-max))
	;; the json pretty printer library changes "{}" to "null" sometimes
	;; and that messes up the dashboard
	;; changing that here
	(beginning-of-buffer)
	(while (search-forward "\"options\": null," nil t)
	  (replace-match "\"options\": {}," nil t))
	(beginning-of-buffer)
	(while (search-forward "\"aliasColors\": null," nil t)
	  (replace-match "\"aliasColors\": {}," nil t))
	(write-region 1 (point-max) backup-file-name)

	;; you can comment out above and use this to not save it "pretty"
	;; (write-region json-contents nil backup-file-name)

	)
      (sit-for 0.3)
      )))

