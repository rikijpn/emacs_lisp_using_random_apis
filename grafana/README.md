# Purpose
1. Irregular backup of grafana dashboards
2. Update of grafana dashboards in bulk

# Pre-requirements
1. grafana auth token with editor permission

# How to get all dashboards list on google chrome (couldn't find how to do it by the API)
1. open the dashboard list
2. press ctrl-shift-j to open the console
3. select the "body" element
4. right click, and select copy outer html
5. copy paste the output
6. grep for 'href="/d/'
7. the uid will the be the part after "/d/" and the url friendly dashboard name afterwards, e.g. the XXXXX part in  "/d/XXXXXX/some-dashboard"
