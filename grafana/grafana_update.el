(load "json")

(defun get-string-from-file (file-path)
  (with-temp-buffer
    (insert-file-contents file-path)
    (buffer-string)))

(defun get-file-and-add-message (file-path)
  "this adds the folder id and a cute message"
  (with-temp-buffer
    (insert-file-contents file-path)
    (search-forward "\"folderId\":" nil t)
    (move-beginning-of-line 1)
    (kill-line 1)
    (yank)
    (beginning-of-buffer )
    (next-line 1)
    (yank)
    (insert "\"message\": \"updated by emacs lisp updater\",")
    (buffer-string)))

(defun call-api-post (base-url params body auth-token)
  (let* (
    	 (url-request-method "POST")
	 (uri-to-use (concat base-url params))
	 (url-request-data body)
	 (url-to-use (url-generic-parse-url uri-to-use))
	 (url-request-extra-headers `(
	 			      ("Authorization" .  ,auth-token)
	 			      ("Accept" .  "application/json")
	 			      ("Content-Type" . "application/json")
	 			      ))
	 (buffer (url-retrieve-synchronously url-to-use)))
    (with-current-buffer buffer
      (progn
	(switch-to-buffer buffer)
	))))

(let ((auth (concat "Bearer "
		    ;; please put a file with the token string with this name in the running path
		    (car (split-string (get-string-from-file "auth.txt")))))
      (api-base-url "https://YOUR-GRAFANA-SERVER/api/")
      (files-to-use (cdddr (directory-files "update"))))
  (dolist (file files-to-use)
    (unless (
	     or (string-equal file ".")
	     (string-equal file ".."))
      (progn 
      	(message "sending request for %s" file)
      	(call-api-post
      	 api-base-url
      	 "dashboards/db"
      	 ;; (get-string-from-file (concat "update/" file)) ;; use this for plain update with no message
	 (get-file-and-add-message (concat "update/" file))
      	 auth
      	 )
      	(sit-for 0.3))))) ;; be nice to the grafana server?

;; no need to update the version number manually
;; grafana accepts only json files with the same/existing version number

;; be careful when using json-pretty-print in emacs, as that messed the "aliasColors" tag, instead of
;; using {} as value, 'null' is added, and the dashboard can no longer be parsed by grafana
