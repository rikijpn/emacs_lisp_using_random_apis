;;(setq auth-token (concat "Basic " (base64-encode-string "<yourusername>:<yourpassword>" )))

(defun call-api-put (base-url params body auth-token)
  (let* (
    	 (url-request-method "PUT")
	 (uri-to-use (concat base-url params))
	 (url-request-data body)
	 (url-to-use (url-generic-parse-url uri-to-use))
	 (url-request-extra-headers `(
	 			      ("Authorization" .  ,auth-token)
	 			      ("Content-Type" . "application/json")
	 			      ))
	 (buffer (url-retrieve-synchronously url-to-use)))
      (with-current-buffer buffer
      	(progn
      	  (switch-to-buffer buffer)))))


;; the code below links (sets a "relate" relationship) all the tickets
;; in the list to the "relation-ticket" ticket

(let* (
       (relation-ticket "XXXX-NNNN") ;; the jira ticket id
       (body
	(format
	 "
{
   \"update\":{
      \"issuelinks\":[
         {
            \"add\":{
               \"type\":{
	         \"name\": \"Relates\",
		 \"inward\": \"relates to\",
		 \"outward\": \"relates to\"
               },
               \"outwardIssue\":{
                  \"key\":\"%s\"
               }
            }
         }
      ]
   }
}
" relation-ticket))
       (ticket-ids
	(list ;; list of tickets you'd like to update here
	 "XXXX-NNNN1"
	 "XXXX-NNNN2"
	 ))

       )
  (dolist (ticket-id ticket-ids)
	  (call-api-put "https://<your-jira-server-here>/jira/rest/api/2/issue/" ticket-id body auth-token)
	  (sit-for 2) ;; let's be nice to the server jira
	  ))


;; add label to tickets
(let* (
       (label-to-set "my-label")
       (body
	(format
	 "{ \"update\":{ \"labels\":[ { \"add\": \"%s\"  } ] }}"
	 label-to-set))
       (ticket-ids
	(list ;; list of tickets you'd like to update here
	 "XXXX-NNNN1"
	 "XXXX-NNNN2"
	 ))

       )
  (dolist (ticket-id ticket-ids)
	  (call-api-put "https://<your-jira-server-here>/jira/rest/api/2/issue/" ticket-id body auth-token)
	  (sit-for 2)
	  )
  )
