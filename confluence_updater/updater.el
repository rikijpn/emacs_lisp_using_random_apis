(setq confluence-auth-header "Basic <yourbase64 username:password code goes here>")


;; this gets a page on confluence based on its page id ( you can check in the
;; "page information" page of your page, as the url will change on your browser
;; pageId=XXXXX, this XXXXX is your page id

(defun get-confluence-page (content-page)
  "gets the page and returns the json as an S-expression"
  (let* ((base-url "https://some.confluence.server.you.are.using/confluence/rest/api/content/")
	 (confluence-complete-page (concat base-url content-page))
    	 (url-request-method "GET")
    	 confluence-response
    	 )
    (let* ((url-to-use (url-generic-parse-url confluence-complete-page))
	   (url-request-extra-headers `(
					("Authorization" .  ,confluence-auth-header)

					("Content-Type" . "application/json")
					))
	   (buffer (url-retrieve-synchronously url-to-use)))
      (with-current-buffer buffer
	(progn
	  ;; (switch-to-buffer buffer) ;; comment this out to debug
	  (search-forward "{\"id" nil t)
	  (beginning-of-line 1)
	  (let*
	      (
	       (ugly-string-beginning (point))
	       (ugly-string-end (buffer-end 1))
	       (json-ugly-string (buffer-substring ugly-string-beginning ugly-string-end)))
	    (setq confluence-response (json-read-from-string json-ugly-string))
	    ))))
    confluence-response))

(defun update-confluence-page-from-file (
					 version
					 confluence-page-id
					 confluence-page-title
					 confluence-page-source-file)
"""
Updates a confluence page based on another file's html code.
The HTML source page file must exist and have valid html code.
You can try creating the page, then adding some random text using
get-confluence-page, and getting the body's value's value in the
returned json expression to begin with.
All newlines and space chars will be deleted.
"""
  (let* ((base-url "https://some.confluence.server.you.are.using/confluence/rest/api/content/")
	 (confluence-complete-page (concat base-url confluence-page-id))
    	 (url-request-method "PUT")
	 (page-contents
	  (progn
	    (with-current-buffer (find-file-noselect confluence-page-source-file)

	      (replace-regexp-in-string
	       "\n"
	       "" 
	       (replace-regexp-in-string
		"\t"
		""
		(buffer-substring-no-properties (point-min) (point-max))))
	      )))
	 
	 (url-request-data 
	  (concat "
{
   \"version\": {\"number\": " (format "%d" (1+  version)) "},
   \"id\": \"" confluence-page-id "\",
   \"type\": \"page\",
   \"status\": \"current\",
   \"title\": \""confluence-page-title"\",
   \"body\": {
     \"storage\": {
       \"value\": \"" page-contents "\",
       \"representation\": \"storage\"
       }
     }
   }
" )))
    (let* ((url-to-use (url-generic-parse-url confluence-complete-page))
    	   (url-request-extra-headers `(
    					("Authorization" .  ,confluence-auth-header)

    					("Content-Type" . "application/json")
    					))
    	   (buffer (url-retrieve-synchronously url-to-use)))
      (with-current-buffer buffer
    	(progn
    	  (switch-to-buffer buffer)
	  )))))


(let ((last-version
       (alist-get 'number (alist-get 'version 
				     (get-confluence-page "NNNNNNNN")))))
  (update-confluence-page-from-file
   confluence-competencies-last-version
 "NNNNNNNN"
 "my confluence page's title"
 "page.html" ;; the html file to use as body for the page
   )
  )
